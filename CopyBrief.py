from selenium import webdriver #Подключение Selenium
from selenium.webdriver.common.by import By # Подключение библиотеки By Selenium
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import time

link = "http://client-app.local:8080/bi/brief/view?brief-id=47236#779478"

browser = webdriver.Chrome()
browser.implicitly_wait(10)
browser.maximize_window()

try:
    browser.get(link)#Открытие сводки Тест Январь
    login = browser.find_element(By.CLASS_NAME, "input").send_keys("i.a.asyamolov")#Ввод логина
    password = browser.find_element(By.NAME, "password").send_keys("8ot5vDTMu")#Ввод пароля
    btnIn = browser.find_element(By.CSS_SELECTOR, "#input-form .primary-button").click()#Клик на кнопку Войти

    #Кнопка "Копировать Сводку"
    CopyBrief = browser.find_element(By.ID, "copy-brief").click()#Клик на кнопку "Копировать сводку"
    allElements = browser.find_elements(By.CLASS_NAME, "Input_radio_3lj")[5].click()#Выбор "Отдельные элементы" 
    singleElements = browser.find_element(By.CLASS_NAME, "SelectBox_content_2KV.SelectElementsBriefUpload_multi_3TO").click()#Клик на "Не выбран"
    selectAllBtn = browser.find_element(By.CSS_SELECTOR, ".SelectBox_controlItems_1Go span").click()#Клик на "Выбрать Все"
    allElements = browser.find_elements(By.CLASS_NAME, "Input_radio_3lj")[5].click()#Клик на "Отдельные элементы"
    allElements = browser.find_elements(By.CLASS_NAME, "Input_radio_3lj")[7].click()#Клик на "Копирование с заменой данных"
    allElements = browser.find_element(By.CSS_SELECTOR, ".SelectBox_content_2KV.SelectElementsBriefUpload_multi_3TO .SelectBox_select_K9V").get_attribute("title")
    print("Выбранные элементы для копирования: " + allElements)
    deleteWidget1 = browser.find_element(By.XPATH, "//*[contains(text(), 'Продолжить')]").click()#Клик на кнопку "Продолжить"

    #Копирование с заменой данных
    step = browser.find_element(By.CLASS_NAME, "SelectElementsBriefUpload_header_1HJ").text
    print("\nЭтап: " + step)
    filters = browser.find_elements(By.CLASS_NAME, "SelectBox_content_2KV.SelectElementsBriefUpload_filterSelect_1jn")[1].click()
    addFilter = browser.find_element(By.CLASS_NAME, "SelectBox_elementsBlock_kcy").click()#Добавление фильтра по полю
    btnDeleted = browser.find_element(By.CLASS_NAME, "SelectElementsBriefUpload_deleteIcon_1Y1")#Проверка, что появилась кнопка удаления фильтра
    el = browser.find_elements(By.CLASS_NAME, "SelectBox_content_2KV.SelectElementsBriefUpload_filterSelect_1jn")
    el1 = browser.find_elements(By.CLASS_NAME, "SelectBox_select_K9V")
    
    #Вывод фильтров
    for i in range(len(el)):
        if el[i]:
            print(el[i].text + el1[i].text)

    clickCopy = browser.find_element(By.CLASS_NAME, "Modal_footer_PfP .Button_btn_12w.Button_primary_3fC").click()#Клик на кнопку "Копировать"

    time.sleep(3)
    #Переход в скопированную сводку(Проверка заголовка,проверка элементов сводки)
    copyBrief = browser.find_element(By.CLASS_NAME, "header.brief-header .responsive-name-text").text
    print(copyBrief)
    checkElements = browser.find_elements(By.CLASS_NAME, "template-title")
    for i in range(len(checkElements)):
        if checkElements[i]:
            print(checkElements[i].text)


finally:
    time.sleep(15)    
    browser.quit()