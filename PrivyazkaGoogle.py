from selenium import webdriver #Подключение Selenium
from selenium.webdriver.common.by import By # Подключение библиотеки By Selenium
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import time

link = "http://client-app.local:8080/bi/brief/view?brief-id=1667#9622"


browser = webdriver.Chrome() # инициализируем драйвер браузера
browser.implicitly_wait(10)# говорим WebDriver искать каждый элемент в течение ... секунд
browser.maximize_window()

try:
    browser.get(link) #открытие страницы сводки Сдвиг периода тест
    login = browser.find_element(By.CLASS_NAME, "input").send_keys("i.a.asyamolov") #Ввод логина
    password = browser.find_element(By.NAME, "password").send_keys("8ot5vDTMu") #Ввод пароля
    btnIn = browser.find_element(By.CSS_SELECTOR, "#input-form .primary-button").click() #клик на кнопку Войти

    #Привязка к Гугл
    browser.find_element(By.ID, "settingGoogle").click()
    time.sleep(2)

    nameBrief = browser.find_element(By.NAME, "name").text #Проверка названия сводки
    print(nameBrief)

    #Проверка чекбоксов для привязки(Все элементы, Текущий элемент, отдельные элементы)
    allElements = browser.find_element(By.CSS_SELECTOR, "[value  = 'allElements']")
    currentElement = browser.find_element(By.CSS_SELECTOR, "[value  = 'currentElement']")
    individualElement = browser.find_element(By.CSS_SELECTOR, "[value  = 'individualElement']")

    #поле ввода для ссылки на целевую таблицу для привязки
    tableReference = browser.find_element(By.CSS_SELECTOR, "[placeholder = 'Ссылка на таблицу']")

    #Существующие связи
    ExistingConnections = browser.find_element(By.XPATH, '//*[contains(text(), "Существующие связи")]')

    #Проверка, что есть кнопка “Создать связь”
    btnCreate = browser.find_element(By.CLASS_NAME, "Button_btn_12w.Button_primary_3fC")

    #Проверка, что есть кнопка копирования адреса аккаунта + проверка, что адрес копируется
    iconCopy = browser.find_element(By.ID, "iconCopy").click()
    tableReference1 = browser.find_element(By.CLASS_NAME, "GoogleUploadSettings_selectBox_3h6.form-control").click()
    tableReference1 = browser.find_element(By.CLASS_NAME, "GoogleUploadSettings_selectBox_3h6.form-control").send_keys(Keys.CONTROL, 'v')
    tableReference1 = browser.find_element(By.CLASS_NAME, "GoogleUploadSettings_selectBox_3h6.form-control").clear()

    #инпуты(Если выбраны инпуты “Все элементы” или “Текущий элемент”, список элементов сводки для выбора заблокирован)
    allElements = browser.find_element(By.CSS_SELECTOR, "[value  = 'allElements']").click()
    listOfitems = browser.find_element(By.CLASS_NAME, "SelectBox_select_K9V.SelectBox_selectDisabled_vXX").is_enabled()
    currentElement = browser.find_element(By.CSS_SELECTOR, "[value  = 'currentElement']").click()
    listOfitems = browser.find_element(By.CLASS_NAME, "SelectBox_select_K9V.SelectBox_selectDisabled_vXX").is_enabled()
    
    #Если выбран инпут “Отдельные элементы”, список разблокирован.(Вывод некоторых выбранных элементов в консоль)
    individualElement = browser.find_element(By.CSS_SELECTOR, "[value  = 'individualElement']").click()
    clickCheckBox = browser.find_element(By.CLASS_NAME, "SelectBox_content_2KV.GoogleUploadSettings_selectBox_3h6").click()
    clickCheckBox = browser.find_elements(By.CLASS_NAME, "SelectBox_title_4p7")
    clickCheckBox[3].click()
    clickCheckBox[4].click()
    print(clickCheckBox[3].text + "\n" + clickCheckBox[4].text)

    #Если элементы не выбраны, то падает уведомление "Элементы сводки не выбраны"
    clearAll = browser.find_element(By.XPATH, '//*[contains(text(), "Очистить все")]').click()
    individualElement = browser.find_element(By.CSS_SELECTOR, "[value  = 'individualElement']").click()
    createConnection = browser.find_element(By.CLASS_NAME, "Button_btn_12w.Button_primary_3fC").click()
    noSelected = browser.find_element(By.XPATH, '//*[contains(text(), "Элементы сводки не выбраны")]')
    print(noSelected.text)

    #Поле “Целевая таблица для привязки”
    currentElement = browser.find_element(By.CSS_SELECTOR, "[value  = 'currentElement']").click()
    tableReference = browser.find_element(By.CLASS_NAME, "GoogleUploadSettings_selectBox_3h6.form-control").send_keys("https://docs.google.com/spreadsheets/d/1QjDaIgLZNou9gG0GfCC4PP7bRIRwM5441oJWMZL92SA/edit#gid=0")
    createConnection = browser.find_element(By.CLASS_NAME, "Button_btn_12w.Button_primary_3fC").click()
    accessTable = browser.find_element(By.XPATH, '//*[contains(text(), "Проверьте доступ к таблице")]')
    print(accessTable.text)
    tableReference = browser.find_element(By.CLASS_NAME, "GoogleUploadSettings_selectBox_3h6.form-control").clear()

     #Если ссылка корректная, падает уведомление “Связь создана” + удаление связи
    currentElement = browser.find_element(By.CSS_SELECTOR, "[value  = 'currentElement']").click()
    tableReference = browser.find_element(By.CLASS_NAME, "GoogleUploadSettings_selectBox_3h6.form-control").send_keys("https://docs.google.com/spreadsheets/d/1QjDaIgLZNou9gG0GfCC4PP7bRIRwM54o1oJWMZL92SA/edit#gid=0")
    createConnection = browser.find_element(By.CLASS_NAME, "Button_btn_12w.Button_primary_3fC").click()
    ConnectionCreated = browser.find_element(By.XPATH, '//*[contains(text(), "Связь создана")]')
    print(ConnectionCreated.text)
    DeletedConnection = browser.find_element(By.XPATH, '//*[contains(text(), "Редактировать")]').click()
    DeletedConnection = browser.find_element(By.CLASS_NAME, "Button_btn_12w.EditGoogleUpload_buttonsMiddle_35Q.EditGoogleUpload_buttonDanger_34q").click()

    #Если поле не заполнено, при создании связи падает уведомление “Укажите корректную ссылку на Google-таблицу”
    time.sleep(2)
    createConnection = browser.find_element(By.CLASS_NAME, "Button_btn_12w.Button_primary_3fC").click()
    ConnectionCreated = browser.find_element(By.XPATH, '//*[contains(text(), "Укажите корректную ссылку на Google-таблицу")]')
    print(ConnectionCreated.text)

finally:
    browser.quit()