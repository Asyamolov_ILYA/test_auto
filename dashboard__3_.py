from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import random

link = "http://client-app.local:8080/main/app?dashboard_id=1#dashboard"
browser = webdriver.Chrome()
browser.maximize_window()
browser.implicitly_wait(4)

#Для корректного запуска скрипта необходимо выбрать все фильтры на дашборде (от города до риэлтора)
try:
    #авторизация
    browser.get(link)
    login = browser.find_element(By.CLASS_NAME, "input").send_keys("r.v.bolvin")
    password = browser.find_element(By.NAME, "password").send_keys("bPHG65skp7")
    btnIn = browser.find_element(By.CSS_SELECTOR, "#input-form .primary-button").click()
    time.sleep(2)

    #Получение текущего дашборда
    dashboardName = browser.find_element(By.CLASS_NAME, "Dashboard_dashboardName_2qL").text
    print("Название дашборда: " + dashboardName)

    #Получение текущих фильтров
    time.sleep(5)
    filters = browser.find_elements(By.CLASS_NAME,'MultiSelect_input_2sq')
    print("\nФильтр \"Город\": " + filters[0].get_attribute("placeholder"))
    print("Фильтр \"РОП\": " + filters[1].get_attribute("placeholder"))
    print("Фильтр \"Менеджер\": " + filters[2].get_attribute("placeholder"))
    print("Фильтр \"Риэлтор\": " + filters[3].get_attribute("placeholder"))

    #Проверка иерархии блокировки фильтров
    deleteManagerFilterButton = browser.find_elements(By.CLASS_NAME, "material-icons.MultiSelect_icon_2LX")
    deleteManagerFilterButton[2].click()
    filters = browser.find_elements(By.CLASS_NAME,'MultiSelect_input_2sq')
    if filters[3].is_enabled():
        print("\nЕсли не выбран менеджер, фильтр \"Риэлтор\" заблокирован: ОК")

    browser.refresh()        

    #Проверка переключения месяца
    print("\nТекущий месяц (до переключения): " + browser.find_element(By.CLASS_NAME, "Dashboard_monthName_3Kh").text)
    previousMonth = browser.find_element(By.CSS_SELECTOR, "[data-icon = 'chevron_left']").click()
    print("Текущий месяц (после переключения): " + browser.find_element(By.CLASS_NAME, "Dashboard_monthName_3Kh").text)
    nextMonth = browser.find_element(By.CSS_SELECTOR, "[data-icon = 'chevron_right']").click()
    print("Текущий месяц: " + browser.find_element(By.CLASS_NAME, "Dashboard_monthName_3Kh").text)

    #Подсказка "Применить"
    prompt = browser.find_elements(By.CSS_SELECTOR, ".col-xs-12.Dashboard_tooltip_3mj span")
    promptString = ""
    for x in range(0,len(prompt)):
        if prompt[x]:
            promptString += prompt[x].text    
    print("\nПри изменении месяца или фильтров появляется подсказка: " + promptString)

    #Поиск в фильтрах
    filters = browser.find_elements(By.CLASS_NAME,'MultiSelect_input_2sq')
    filters[0].click()
    filters[0].send_keys("тюме")
    searchCity = browser.find_elements(By.CLASS_NAME, "MultiSelectList_value_2kj")
    print("\nГородов, удовлетворяющих поиску: " + str(len(searchCity)-1))
    searchCity[0].click() #Элемент "Все"
    filters = browser.find_elements(By.CLASS_NAME,'MultiSelect_input_2sq')
    print("Текущее количество городов в фильтре: " + filters[0].get_attribute("placeholder"))

    #Уволенные сотрудники
    filters[1].click()
    time.sleep(5)
    dismissEmployeeIcon = browser.find_element(By.CLASS_NAME, "MultiSelectList_iconDeleteUsers_2nT")
    dismissEmployeeIcon.click()
    time.sleep(3)
    #dismissEmployeeList = browser.find_elements(By.CLASS_NAME, "MultiSelectList_valueBlock_2TB")
    dismissEmployeeList = browser.find_elements(By.CSS_SELECTOR, ".MultiSelectList_firstList_25i")#получаем ссылки на 2 списка с таким классом
    list = dismissEmployeeList[1].find_elements(By.CSS_SELECTOR, "span.MultiSelectList_value_2kj[title]")#находим все спаны с классом 
    print("\nУволенные РОПы:")
    for i in range(0,len(list)):
        if list[i]:
            print(list[i].text)

    #сохранение фильтров
    acceptButton = browser.find_element(By.XPATH, "//button[text() = 'Применить']").click()
    savedSearchButton = browser.find_element(By.CSS_SELECTOR, "span.Dashboard_dashboardName_2qL:nth-child(1)").click()
    saveSearch = browser.find_element(By.CSS_SELECTOR, "div.Dashboard_dashboadListItem_lUh:nth-child(1)").click()
    #добавить обработку всплывающего уведомления
    #сброс фильтров

finally:
    time.sleep(5)
    browser.quit()